﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace PagJuego
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "IndexLogin", id = UrlParameter.Optional }
            );

			routes.MapRoute(
				name: "Login",
				url: "Home/iniciosesion",
				defaults: new { controller = "Home", action = "iniciosesion" }
			);

			routes.MapRoute(
			name: "Index",
			url: "Home/Index",
			defaults: new { controller = "Home", action = "Index" }
		);
			routes.MapRoute(
				name: "Info",
				url: "Home/Info",
				defaults: new { controller = "Home", action = "Info" }
			);

			routes.MapRoute(
				name: "Descarga",
				url: "Accesos/Descarga",
				defaults: new { controller = "Home", action = "Descarga" }
			);
			routes.MapRoute(
				name: "Registro",
				url: "Acessos/Registro",
				defaults: new { controller = "Home", action = "Registro" }
			);

		}
	}
}
