﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PagJuego.Models
{
    public class IndexController : Controller
    {
		// GET: home
		public ActionResult Index(Usuario oUsuario)
		{
			ViewBag.Usuario = oUsuario;
			return View();
		}

		public ActionResult Login()
		{
			// Código para mostrar la vista de inicio de sesión
			return View();
		}


		public ActionResult Info()
		{
			return View();
		}

		public ActionResult Descarga()
		{
			return View();
		}
		// GET: home/Details/5
		public ActionResult Details(int id)
        {
            return View();
        }

        // GET: home/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: home/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: home/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: home/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: home/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: home/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
